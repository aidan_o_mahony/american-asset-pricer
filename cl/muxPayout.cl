#include "include/optPricer.h"

#pragma OPENCL EXTENSION cl_intel_channels : enable
//__attribute__((max_global_work_dim(0)))
//__attribute__((autorun))
kernel void muxPayout ( __global double * restrict option_value )
{	

	type_payout payout;
	bool control = 0;
	bool finished = 0;
	while (finished == 0) {
		if (control == 0) {
			payout    = read_channel_intel(payoutChannel);
			control   = payout.treeStatus & ( 1 << TREE_END_LINE_BIT);
		}
		else {
		  payout    = read_channel_intel(payoutChannelReturn);
			//finished  = payout.last | payout.finished;
			finished  = (payout.treeStatus & ( 1 << TREE_LAST_BIT)) | (payout.treeStatus & ( 1 << TREE_FINISHED_BIT));
			printf ("payout status %x\n",payout.treeStatus);
		}
		if (finished == 0) {
			write_channel_intel(euPricerOutChannel[0], payout);
		}
	}
	option_value[0] = payout.payout;
	write_channel_intel(finishedChannel, 1);

	printf(".................................muxout exit........................\n");
}

