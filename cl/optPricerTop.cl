#pragma OPENCL EXTENSION cl_intel_channels : enable

#include "include/optPricer.h"
#include "include/optPricerChannels.h"
#include "producer.cl"
#include "stockPrice.cl"
#include "dataPreSorter.cl"
#include "payout.cl"
#include "writeMultiTierBuffer.cl"
#include "readMultiTierBuffer.cl"
#include "computeValue.cl"
#include "dataSorter.cl"
#include "channelDDR0.cl"
#include "channelDDR1.cl"
#include "muxTreeDepth.cl"




