#include "include/optPricer.h"


#pragma OPENCL EXTENSION cl_intel_channels : enable
__attribute__((max_global_work_dim(0)))
kernel void  stockPrice  ( )
{

	// Variables
	//printf ("stock Price Entry \n");
	
	double                __attribute__((register)) init_step[NUM_ASSETS][NUM_ASSETS];
	double                __attribute__((register)) init_shares[NUM_ASSETS];
	type_value            __attribute__((register)) stockValue;
	type_stockPriceTree   __attribute__((register)) stockTree[NUM_ASSETS];


	unsigned short treeDepth;
	bool finished;  
	finished = 0;
	unsigned short currentIndex = 0;
	unsigned short nextIndex = 0;
	unsigned short tempIndex;
	bool moveSideways = 0;
	unsigned char i, j;
	for (i = 0; i < NUM_ASSETS; i++) {
		for (j = 0; j < NUM_ASSETS; j++) {
			init_step[i][j] = read_channel_intel(init_step_channel);
			//printf ("init_step: %.24f \n",init_step[i][j]);
		}
	}
	for (i = 0; i < NUM_ASSETS; i++) {
		init_shares[i] = read_channel_intel(init_shares_channel);
		stockValue.value[i] = init_shares[i];
		//printf ("init_shares: %.24f \n",init_shares[i]);
	}
	treeDepth   = read_channel_intel(tree_depth_channel);
	//printf ("treeDepth: %d \n",treeDepth);

	stockValue.finished = 0;

	write_channel_intel (stockValueChannel, stockValue);
	//printf ("stock value: %.24f , %.24f , %.24f , %.24f \n", stockValue.value[0],stockValue.value[1],stockValue.value[2],stockValue.value[3] );
	#pragma unroll NUM_ASSETS
	for (i = 0; i < NUM_ASSETS; i++) {
		stockTree[currentIndex].stockPrice[i] = init_shares[i];
	}
	stockTree[0].numberOfBranches = treeDepth;
	stockTree[0].currentBranch    = 1;
	stockTree[0].returnBranch     = 1;
	stockTree[0].index            = 0;
	while (1) {
		if (moveSideways) {
			#pragma unroll NUM_ASSETS
			for (i = 0; i < NUM_ASSETS; i++) {
				stockTree[currentIndex].stockPrice[i] =stockTree[currentIndex-1].stockPrice[i] * init_step[i][currentIndex];
			}
		} else {
			#pragma unroll NUM_ASSETS
			for (i = 0; i < NUM_ASSETS; i++) {
				stockTree[currentIndex].stockPrice[i] *= init_step[i][currentIndex];
			}
		}
		#pragma unroll NUM_ASSETS
		for (i = 0; i < NUM_ASSETS; i++) {
			stockValue.value[i] = stockTree[currentIndex].stockPrice[i];
		}
		write_channel_intel (stockValueChannel, stockValue);
		//printf ("stock value: %.24f , %.24f , %.24f , %.24f \n", stockValue.value[0],stockValue.value[1],stockValue.value[2],stockValue.value[3] );
		if (finished) break;
		moveSideways = 1;
		nextIndex = currentIndex + 1;
		if (currentIndex < NUM_BRANCHES -2) { 
			stockTree[nextIndex].numberOfBranches = stockTree[currentIndex].currentBranch;
			stockTree[nextIndex].currentBranch    = 1;
			stockTree[nextIndex].index            = stockTree[currentIndex].currentBranch + 1;
			if ((stockTree[nextIndex].numberOfBranches == 1) || 
                            (nextIndex == (NUM_BRANCHES-2)) ||
                            (stockTree[nextIndex].numberOfBranches == stockTree[nextIndex].currentBranch)){
				stockTree[nextIndex].returnBranch = stockTree[currentIndex].returnBranch;
			} else {
				stockTree[nextIndex].returnBranch = nextIndex + 1;
			}
			currentIndex = nextIndex;
		} 

		else if (stockTree[currentIndex].numberOfBranches != stockTree[currentIndex].currentBranch) { 
			++stockTree[currentIndex].currentBranch;	
		moveSideways = 0;
			if (stockTree[currentIndex].currentBranch == (treeDepth) && (stockTree[currentIndex].numberOfBranches == treeDepth)) {
				finished = 1;
				stockValue.finished = 1;
			}
		} 

                else { 
			moveSideways = 0;
			if (stockTree[currentIndex].currentBranch == (treeDepth)) {
				finished = 1;
				stockValue.finished = 1;
			} else {
				currentIndex = stockTree[currentIndex].returnBranch - 1;
				++stockTree[currentIndex].currentBranch;
				if (currentIndex > 0 && ( stockTree[currentIndex].numberOfBranches == stockTree[currentIndex].currentBranch )) {
					stockTree[currentIndex].returnBranch = stockTree[currentIndex - 1].returnBranch;
				}
			}
		}
	}
	//printf ("EXIT STOCKPRICE\n");
}


