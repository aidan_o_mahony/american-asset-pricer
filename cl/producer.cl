
__kernel void producer ( __global double* restrict init_step,  // init_step        // 0
                         __global double* restrict strikevect, // strike           // 1
                         __global double* restrict istepnext,  // init_step_next   // 2
                         __global double* restrict mcoeef,     // mult_coeff       // 3
                         __global double* restrict ishares,    // init shares      // 4
                         __global unsigned short* restrict mx_mn,                  // 5
                         __global double* restrict csh,                           // 6
                         __global unsigned short* restrict cput,                   // 7
			                   unsigned short treeDepth)
{


	// Need to understand the ordering.
	// strike[NUM_ASSETS]         = {strikevect[0],strikevect[1]           };
	// init_step_next[NUM_ASSETS] = {istepnext[0], istepnext[1]            };
	// mult_coeff[NUM_BRANCHES]   = {mcoeef[2],    mcoeef[1],    mcoeef[0] };
	// init_shares[NUM_ASSETS]    = {ishares[0],   ishares[1]              };
	//  init_step[0][0] = init_step1[0]; init_step[0][1] = init_step1[1];
	//  init_step[1][0] = init_step2[0]; init_step[1][1] = init_step2[1];

	unsigned char i;
	unsigned char j;
	unsigned char k;
	for (i = 0; i < NUM_ASSETS; i++) { 
		for (k = 0; k < NUM_ASSETS; k++) {
			write_channel_intel(init_step_channel,init_step[i* NUM_ASSETS + k]);
		}
	}

	for (i = 0; i < NUM_ASSETS; i++) {
		write_channel_intel(strike_channel,strikevect[i]);
	}


	for (i = 0; i < NUM_ASSETS; i++) {
		//init_step_next[i] = istepnext[i];
	}

	for (i = 0; i < NUM_ASSETS; i++) {
		write_channel_intel(init_shares_channel,ishares[i]);
	}

	for (i = 0; i < NUM_BRANCHES; i++) {
		for (k = 0; k < COMPUTE_DEPTH; k++) {
			write_channel_intel(mult_coeff_channel[k],mcoeef[i]);
		}
	}

	//for (i = 0; i < NUM_ASSETS; i++) {
		write_channel_intel(tree_depth_channel,treeDepth);
	//}
	write_channel_intel(max_min_channel, *mx_mn);
	write_channel_intel(cash_channel, *csh);
	write_channel_intel(call_put_channel, *cput);
	write_channel_intel(muxTreeDepthChannel, (treeDepth));

	bool waiting = read_channel_intel(finishedChannel);
	//printf("EXIT PRODUCER\n");
}

