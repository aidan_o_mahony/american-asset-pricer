
#ifndef _OPT_PRICER_H
#define	_OPT_PRICER_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <quadmath.h>

#define NUM_ASSETS 2
#define NUM_BRANCHES 3

/* ========= */
/* Libraries */
/* ========= */
/*#include "/export/home/postgrad/christians/dmalloc/dmalloc-5.4.2/dmalloc.h" */
#include "generic_fifo.h"
#include "fp2bin.h"

/* =========== */
/* Definitions */
/* =========== */
#define STR_MAX	(500)
#define ushort unsigned short
#define ulong unsigned long


#define PRECISION 64 //32 64 128   //single, double. quad
#if PRECISION==32
#define float_type float
#define MAX_FUNC fmaxf
#define MOD_FUNC fmodf
#define POW_FUNC powf
#define EXP_FUNC expf
#define SQRT_FUNC sqrtf
#define ABS_FUNC fabsf
#define CAST *(unsigned int*)
#define PRNT_FRM "f"
#define PRNT_PREC 16
#elif  PRECISION==64
#define float_type double
#define MAX_FUNC fmax
#define MOD_FUNC fmod
#define POW_FUNC pow
#define EXP_FUNC exp
#define SQRT_FUNC sqrt
#define ABS_FUNC fabs
#define CAST *(unsigned long long int*)
#define PRNT_FRM "lf"
#define PRNT_PREC 20
#elif  PRECISION==128
#define float_type __float128
#define MAX_FUNC fmaxq
#define MOD_FUNC fmodq
#define POW_FUNC powq
#define EXP_FUNC expq
#define SQRT_FUNC sqrtq
#define ABS_FUNC fabsq
#define CAST *(unsigned __INT128*)
#define PRNT_FRM "Qf"
#define PRNT_PREC 20
#else 
#error precison not supported
#endif

#define CONFIG_FILE	"opt_pricer.config"	/* Default name of configuration file 	*/


/* ============ */
/* Enumerations */
/* ============ */
/* update_status */
//typedef enum {
// 	OVERWRITE = 0,
// 	UPDATE,
// 	ADD
//} update_status;


/* ========== */
/* Structures */
/* ========== */

/* config_set_struct */
typedef struct config_set_struct {
  ushort OPT_EUROPEAN;
  ushort OPT_AMERICAN;
  ushort CALL_PUT;
  float_type T;
  ushort NUM_LAYERS;
  ushort VERBOSE;
  ushort NUM_DIM;
  ushort MAX_MIN;
  float_type RATE;
  float_type EXP_RATE;
  float_type CASH;	
  float_type **share_mult_vect;
  float_type *eu_prob_vect;
  float_type *strike_prob_vect;
  float_type *init_share_value_vect;
  float_type **sigma_matrix;
} config_set;

typedef struct opt_price_params_t {
    int max_min;
    int num_layers;
    double init_share[NUM_ASSETS]; // done
    double strike[NUM_ASSETS]; // done
    double cash;
    double init_step[NUM_ASSETS][NUM_ASSETS]; // done
    double init_step_next[NUM_ASSETS];
    double mult_coeff[NUM_BRANCHES];
    double shares_coeff[NUM_ASSETS];
    unsigned short call_put;
} opt_price_params;

typedef struct opt_price1_params_t {
    int max_min;
    int num_layers;
    double init_share[NUM_ASSETS]; // done
    double strike[NUM_ASSETS]; // done
    double cash;
    double init_step[NUM_ASSETS][NUM_ASSETS]; // done
    double init_step_next[NUM_ASSETS];
    double mult_coeff[NUM_BRANCHES];
    double shares_coeff[NUM_ASSETS];
    unsigned short call_put;
} opt_price1_params;


/* ================ */
/* Global Variables */
/* ================ */
//extern const char *DECODER_TYPE_NAME[];

/* =================== */
/* Function prototypes */
/* =================== */
void usage_opt_pricer();
void assert_mem(void *ptr_to_new_mem);
void init_fifos(generic_fifo *fifo_shares,generic_fifo **fifo_value_vect,generic_fifo *fifo_partis,config_set *conf_set, opt_price_params *opp);
void init_fifos_again(generic_fifo *fifo_shares,generic_fifo **fifo_value_vect,generic_fifo *fifo_partis,opt_price_params *opp);
void gen_partitions_rec(ushort* partition,  ushort pos, float_type *share,generic_fifo *fifo,generic_fifo **fifo_value_vect,generic_fifo *fifo_shares,config_set *conf_set);
void gen_partitions_rec_again(ushort* partition,  ushort pos, float_type *share,generic_fifo *fifo,generic_fifo **fifo_value_vect, generic_fifo *fifo_shares,opt_price_params *opp);
void print_partition(ushort *partition,ushort num_dim, char *heading);
void print_shares(float_type *shares,ushort num_dim, char *heading);
float_type  compute_value(float_type *old_val, float_type *shares,config_set *conf_set);
float_type  compute_value_init(float_type *old_val, float_type *shares,config_set *conf_set);
float_type compute_payoff(float_type *shares,config_set *conf_set);
float_type max_array(float_type *array,ushort dim);
float_type min_array(float_type *array,ushort dim);
const char *byte_to_binary(int x);
void compute_d_and_q(float_type **sigma_matrix,float_type **d_matrix,float_type *q_vector, float_type rate, float_type dt,ushort num_dim,ulong T);
void print_gsl_vector(gsl_vector *v);
void print_gsl_matrix(gsl_matrix *m);
void read_config(FILE *fid,config_set *conf_set, char * config_file);

float_type compute_payoff1(float_type *shares,opt_price_params *opp);
void  prinf_float_type(char *head,float_type val,ushort frac_digits);
void prinf_float_type_hex(char *head,float_type val);
void  fprintf_float_type(FILE *fid,char *head,float_type val,ushort frac_digits);
void print_float_type_matrix(float_type  **m,const ulong row,const  ulong col, char *head);
  void print_float_type_vector(float_type *v,ulong dim,char * head);
void convert_to_gls_mat(float_type **m,ulong rows,ulong cols,gsl_matrix *m_gsl);

void load_config_file(char *config_file, config_set *conf_set);
/*
#ifdef DDMALLOC
#include "dmalloc.h"
#endif
*/
#endif
