#include "include/optPricer.h"

#pragma OPENCL EXTENSION cl_intel_channels : enable
kernel void payoutKernel ( __global double * restrict option_value, short treeDepth )
{	

double __attribute__((register)) strike_prob_vect[NUM_ASSETS];
unsigned short max_min;
double cash;
unsigned short call_put;

//global unsigned short treeDepth;

	//float __attribute__((register)) strike_prob_vect[NUM_ASSETS];
	//nsigned short max_min = *mx_mn;
	//float         cash = *csh;
	type_value  stockValue;
	type_payout payout;
	bool  control = 0;
	bool  finished = 0;
	unsigned short treePreSortStatus;

	for (unsigned char i = 0; i < NUM_ASSETS; i++) {
		strike_prob_vect[i] = read_channel_intel(strike_channel);
	}
	max_min  = read_channel_intel(max_min_channel);
	cash     = read_channel_intel(cash_channel);
	call_put = read_channel_intel(call_put_channel);
	payout.treeStatus  = (treeDepth & TREE_DEPTH_MASK) | (1 << TREE_START_LINE_BIT);
	while (control == 0) {
		double maxTemp = 0;
		double minTemp = 10000000;
		double fTemp;
		double delta;
		stockValue        = read_channel_intel(stockValueChannel);
		treePreSortStatus = read_channel_intel(treePreSortStatusChannel);
		
		#pragma unroll NUM_ASSETS
		for (unsigned char i = 0; i < NUM_ASSETS; i++) {
			if (call_put == 1) {
				delta  =  stockValue.value[i] - strike_prob_vect[i];
			} else {
				delta  =  strike_prob_vect[i] - stockValue.value[i];
			}
			if (maxTemp < delta) { maxTemp = delta;} 
			if (minTemp > delta) { minTemp = delta;} 			
			if ((max_min==1 && call_put ==1) || (max_min==0 && call_put == 0)) {
				fTemp = maxTemp;
			} else {
				fTemp = minTemp;
			}
		}
		double maxVal = fTemp;
		if (fTemp < cash) {
			maxVal = cash;
		}
		control           = stockValue.finished;
		if (control == 1) {
			payout.treeStatus = (treeDepth & TREE_DEPTH_MASK) | (1 << TREE_END_LINE_BIT);
		}
		payout.payout     = maxVal;
		payout.treePreSortStatus = treePreSortStatus;
		// COMPUTE Payout.
		write_channel_intel(euPricerOutChannel[0], payout);
		//printf ("Payout: %.24f \n", maxVal);
		payout.treeStatus = (treeDepth & TREE_DEPTH_MASK);
	}
	finished = 0;
	//printf ("Exit...\n");
	while (finished == 0) {
		payout    = read_channel_intel(payoutChannelReturn);
		//finished  = payout.last | payout.finished;
		finished  = (payout.treeStatus & ( 1 << TREE_LAST_BIT)) | (payout.treeStatus & ( 1 << TREE_FINISHED_BIT));
		//if (finished == 0) {
			write_channel_intel(euPricerOutChannel[0], payout);
		//}
		//printf ("----------------------------------------Payout: %f Tree Status: %04x  PRE SORTER TREE: %04x FINISHED: %d\n", payout.payout, payout.treeStatus, payout.treePreSortStatus, finished);
	}
	option_value[0] = payout.payout;
	write_channel_intel(finishedChannel, 1);
}

