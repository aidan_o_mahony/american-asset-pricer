#include "include/optPricer.h"


#pragma OPENCL EXTENSION cl_intel_channels : enable
__attribute__((max_global_work_dim(0)))
__attribute__((num_compute_units(COMPUTE_DEPTH,1)))
__attribute__((autorun))
kernel void computeValue ( )
{	

	double mult_coeff[MAX_NUM_ASSETS];	
	type_payout payout[MAX_NUM_ASSETS+1];
	type_payout euPayout;
	bool  control = 0;
	int processingUnit = get_compute_id(0);

	for (unsigned char i = 0; i < NUM_BRANCHES; i++) {
		mult_coeff[i] = read_channel_intel(mult_coeff_channel[processingUnit]);
		//printf ("Read mult_coeff: %.24f \n", mult_coeff[i]);
	}

	while (control == 0) {
		double sumP  =  0;
		
		// These will happen all the time Minimum Assets of 2
		payout[0] = read_channel_intel(euPricerInChannel_0[processingUnit]);
		payout[1] = read_channel_intel(euPricerInChannel_1[processingUnit]);
		payout[2] = read_channel_intel(euPricerInChannel_2[processingUnit]);
		if (NUM_ASSETS > 2) {
			payout[3] = read_channel_intel(euPricerInChannel_3[processingUnit]);
		}
		if (NUM_ASSETS > 3) {
			payout[4] = read_channel_intel(euPricerInChannel_4[processingUnit]);
		}
		if (NUM_ASSETS > 4) {
			payout[5] = read_channel_intel(euPricerInChannel_5[processingUnit]);
		}
		if (NUM_ASSETS > 5) {
			payout[6] = read_channel_intel(euPricerInChannel_6[processingUnit]);
		}
		if (NUM_ASSETS > 6) {
			payout[7] = read_channel_intel(euPricerInChannel_7[processingUnit]);
		}
		if (NUM_ASSETS > 7) {
			payout[8] = read_channel_intel(euPricerInChannel_8[processingUnit]);
		}
		if (NUM_ASSETS > 8) {
			payout[9] = read_channel_intel(euPricerInChannel_9[processingUnit]);
		}
		if (NUM_ASSETS == 2) {
			sumP  =  (payout[0].payout * mult_coeff[0]) +  
			         (payout[1].payout * mult_coeff[1]) +  
			         (payout[2].payout * mult_coeff[2]);
		}
		// These will happen IF NUM_ASSETS >=3
		if (NUM_ASSETS == 3) {
			sumP  =  (payout[0].payout * mult_coeff[0]) +  
			         (payout[1].payout * mult_coeff[1]) +  
			         (payout[2].payout * mult_coeff[2]) +
						   (payout[3].payout * mult_coeff[3]);
		}
		// These will happen IF NUM_ASSETS >=4
		if (NUM_ASSETS == 4) {
			sumP  =  (payout[0].payout * mult_coeff[0]) +  
			         (payout[1].payout * mult_coeff[1]) +  
			         (payout[2].payout * mult_coeff[2]) +
						   (payout[3].payout * mult_coeff[3]) +
			         (payout[4].payout * mult_coeff[4]);
		}
		// These will happen IF NUM_ASSETS >=5
		if (NUM_ASSETS == 5) {
			sumP  =  (payout[0].payout * mult_coeff[0]) +  
			         (payout[1].payout * mult_coeff[1]) +  
			         (payout[2].payout * mult_coeff[2]) +
						   (payout[3].payout * mult_coeff[3]) +
			         (payout[4].payout * mult_coeff[4]) +
			         (payout[5].payout * mult_coeff[5]);
		}
		// These will happen IF NUM_ASSETS >=6
		if (NUM_ASSETS == 6) {
			sumP  =  (payout[0].payout * mult_coeff[0]) +  
			         (payout[1].payout * mult_coeff[1]) +  
			         (payout[2].payout * mult_coeff[2]) +
						   (payout[3].payout * mult_coeff[3]) +
			         (payout[4].payout * mult_coeff[4]) +
			         (payout[5].payout * mult_coeff[5]) +
			         (payout[6].payout * mult_coeff[6]);
		}
		// These will happen IF NUM_ASSETS >=7
		if (NUM_ASSETS == 7) {
			sumP  =  (payout[0].payout * mult_coeff[0]) +  
			         (payout[1].payout * mult_coeff[1]) +  
			         (payout[2].payout * mult_coeff[2]) +
						   (payout[3].payout * mult_coeff[3]) +
			         (payout[4].payout * mult_coeff[4]) +
			         (payout[5].payout * mult_coeff[5]) +
			         (payout[6].payout * mult_coeff[6]) +
			         (payout[7].payout * mult_coeff[7]);
		}
		// These will happen IF NUM_ASSETS >=8
		if (NUM_ASSETS == 8) {
			sumP  =  (payout[0].payout * mult_coeff[0]) +  
			         (payout[1].payout * mult_coeff[1]) +  
			         (payout[2].payout * mult_coeff[2]) +
						   (payout[3].payout * mult_coeff[3]) +
			         (payout[4].payout * mult_coeff[4]) +
			         (payout[5].payout * mult_coeff[5]) +
			         (payout[6].payout * mult_coeff[6]) +
			         (payout[7].payout * mult_coeff[7]) +
			         (payout[8].payout * mult_coeff[8]);
		}
		
		
		
		euPayout =  payout[0];
		// IF finished = 1 
		if (( (euPayout.treeStatus >> TREE_FINISHED_BIT) & 0x0001) == 0)  euPayout.payout = sumP;

		//euPayout.endLine = payout[NUM_BRANCHES-1].endLine;
		if (( (payout[NUM_BRANCHES-1].treeStatus >> TREE_END_LINE_BIT) & 0x0001) == 0) {
			euPayout.treeStatus     &= (~(1 << TREE_END_LINE_BIT));
		} else {
			euPayout.treeStatus     |=   (1 << TREE_END_LINE_BIT);
		}

		//printf ("...........PUNIT: %d, Payout: %f treeStatus: %x preSorterStatus: %x\n", processingUnit, payout[0].payout, euPayout.treeStatus, euPayout.treePreSortStatus);
		//printf ("Option Price: %.24f , treeStatus: %x \n", sumP, euPayout.treeStatus);
		// COMPUTE Payout.
		write_channel_intel(euPricerOutChannel[processingUnit+1], euPayout);
		//if (euPayout.last || euPayout.finished) {
		if ( (((euPayout.treeStatus >> TREE_LAST_BIT) | (euPayout.treeStatus >> TREE_FINISHED_BIT)) & 0x0001) != 0 ) {
		//if ( (((euPayout.treeStatus >> TREE_LAST_BIT) ) & 0x0001) != 0 ) {
			break;
		}
	}
	//printf ("compute Value Exit...Node= %d, TreeStatus= %x\n", processingUnit, euPayout.treeStatus);
}

