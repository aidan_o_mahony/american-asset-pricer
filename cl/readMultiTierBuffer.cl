#include "include/optPricer.h"

#pragma OPENCL EXTENSION cl_intel_channels : enable
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))

kernel void readMultiTierBuffer ( )
{	
  type_payout x, y;

	char finished = 0;
	char activeReadBuffer  = 3;


	while (1) {
		if ( (activeReadBuffer &0x3) == 3) {
			activeReadBuffer = read_channel_intel(nextBuff);
		}
		if ((activeReadBuffer & 0x3) == 0) {
			y = read_channel_intel(readPayoutBufferChannel[0]);
		}
		if ( (activeReadBuffer & 0x3) == 1) {
			y = read_channel_intel(readPayoutBufferChannel[1]);
		}
		if ( (activeReadBuffer & 0x03) == 2) {
			y = read_channel_intel(multiBufferChannel2);
		  if ( ((y.treeStatus >> TREE_START_LINE_BIT) & 0x0001) && ((y.treeStatus >> TREE_END_LINE_BIT) & 0x0001) == 0x01) {
		  	activeReadBuffer = 3;
		  }
		}	
		write_channel_intel(payoutChannelReturn, y);

		if ( ((activeReadBuffer & 0x03) != 2) && (((y.treeStatus >> TREE_END_LINE_BIT) & 0x01) == 0x1) ) {
			activeReadBuffer = 3;
		}

		/* End of Reading from Internal/External Buffers */
	}
	//printf("EXIT BUFFER\n");
}
