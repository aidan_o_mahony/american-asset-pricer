 
// #include <config.h>
 #include <stdlib.h>
 #include <gsl/gsl_math.h>
 #include <gsl/gsl_vector.h>
 #include <gsl/gsl_matrix.h>
 #include <gsl/gsl_linalg.h>
 #include "opt_pricer.h"

 #define REAL float_type
///////////////////////////
// functions prototypes //
/////////////////////////

float_type quiet_sqrt (float_type x);
int linalg_cholesky_decomp_gen (float_type **A, size_t rows, size_t cols);
int linalg_HH_svx_gen (float_type **A, const ulong rowA,const ulong colA, float_type *x, const ulong dimX);
void matrix_mult_gemm(float_type **R, float_type **A, float_type **B, const ulong rowR,const ulong colR, const ulong rowA,const ulong colA, const ulong rowB,const ulong colB);
