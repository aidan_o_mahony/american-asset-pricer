#include "opt_pricer.h"
#include "lin_alg_gen.h"
#include "inverse.h"
#include "utils.h"


/* usage_ldpc_sim */
void usage_opt_pricer() {
    fprintf(stderr,"================================\n");
    fprintf(stderr,"OPTION PRICER calculator:\n");
    fprintf(stderr,"================================\n");
    fprintf(stderr,"Usage:   opt_pricer  [<config_file>]\n");
    fprintf(stderr,"Option:  <config_file>  Use <config_file> configuration file.\n");
    fprintf(stderr,"                        Default is 'opt_pricer.config'\n\n");
    exit(EXIT_FAILURE);
}
/* assert_mem */
/* Checks to see if memory has been allocated */
void assert_mem(void *ptr_to_new_mem) {
    if(ptr_to_new_mem == NULL) {
        fprintf(stderr, "\nERROR: Could not allocate new memory.\n");
        exit(EXIT_FAILURE);
    } else {
        return;
    }
}

void print_shares(float_type *shares,ushort num_dim, char *heading){
    char tmp[20];
    char tmp2[20];
    for (int i=0;i<num_dim;i++){
        strcpy(tmp2,heading);
        sprintf(tmp,"\n %s  shar[%d] ",tmp2,i);
        prinf_float_type(tmp,shares[i],PRNT_PREC);
        //    printf("%g,",shares[i]);
    }
}


void print_partition(ushort *partition,ushort num_dim, char *heading){
    printf("\n %s part=",heading);
    for (int i=0;i<num_dim+1;i++){
        printf("%u,",partition[i]);
    }
}

float_type max_array(float_type *array,ushort dim){
    float_type maxf=array[0];
    for(int i=0;i<dim;i++){
        if (array[i]>maxf){maxf=array[i];}
    }
    return maxf;
}

float_type min_array(float_type *array,ushort dim){
    float_type minf=array[0];
    for(int i=0;i<dim;i++){
        if (array[i]<minf){minf=array[i];}
    }
    return minf;
}


const char *byte_to_binary(int x)
{
    static char b[6];
    b[0] = '\0';

    int z;
    for (z = 32; z > 0; z >>= 1)
    {
        strcat(b, ((x & z) == z) ? "1" : "0");
    }

    return b;
}

void read_config(FILE *fid,config_set *conf_set, char *config_file){
    char line[STR_MAX]				= {""};
    char rubbish[STR_MAX]			= {""};
    char temp_string[STR_MAX]		= {""};
    char variable_name[STR_MAX]		= {""};
    char variable_value[STR_MAX]	= {""};
    int i,j,num_arg						= 0;

    size_t string_size				= 0;
    ushort VERBOSE		= 0;
    ushort OPT_EUROPEAN=0;
    ushort CALL_PUT=0;
    ushort OPT_AMERICAN=0;
    ushort NUM_LAYERS=0;
    ushort NUM_DIM=0;
    ushort MAX_MIN=0;

    float_type CASH=0;
    float_type RATE=0;
    float_type EXP_RATE=0;
    float_type T=0;

    float_type **share_mult_vect=NULL;
    float_type *eu_prob_vect=NULL;
    float_type *strike_prob_vect=NULL;
    float_type *init_share_value_vect=NULL;
    float_type **sigma_matrix=NULL;

    /* Read config file line-by-line */
    while(!feof(fid)) {
        /* read line */
        fgets(temp_string, STR_MAX, fid);

        /* remove comments */
        string_size = strcspn(temp_string,"#");
        strncpy(line, temp_string, string_size);
        line[string_size] = '\0';

        /* remove leading spaces */
        string_size = strspn(line, " \t\n");	/* length of leading whitespaces */
        strcpy(temp_string, &line[string_size]);

        /* read variable name and value */
        num_arg = sscanf(temp_string,"%[^ \t]%[ \t]%[^ \t\n]",variable_name,rubbish,variable_value);
        if(num_arg == 0 || num_arg == EOF) {
            continue;
        } else if(num_arg != 3) {
            fprintf(stderr, "\nERROR: Failed to read properly from %s file.\n",config_file);
            exit(EXIT_FAILURE);
        }

        /* Large if else structure */
        if( !strcmp(variable_name,"OPT_EUROPEAN") ) {
            OPT_EUROPEAN = (ushort)atol(variable_value);
        } else if( !strcmp(variable_name,"CALL_PUT") ) {
            CALL_PUT = (ushort)atol(variable_value);
        } else if( !strcmp(variable_name,"OPT_AMERICAN") ) {
            OPT_AMERICAN = (ushort)atol(variable_value);
        } else if( !strcmp(variable_name,"NUM_LAYERS") ) {
            NUM_LAYERS   = (ulong)atol(variable_value);
        } else if( !strcmp(variable_name,"NUM_DIM") ) {
            NUM_DIM      = (ushort)atol(variable_value);
            //Alloc vector for shares coeff etc
            share_mult_vect = (float_type **)malloc(NUM_DIM*sizeof(float_type *));
            assert_mem(share_mult_vect);
            for (i=0;i<NUM_DIM;i++){
                share_mult_vect[i] = (float_type *)malloc((NUM_DIM+1)*sizeof(float_type));	
                assert_mem(share_mult_vect[i]);
            }
            eu_prob_vect = (float_type *)calloc(NUM_DIM+1,sizeof(float_type));
            assert_mem(eu_prob_vect);
            strike_prob_vect = (float_type *)calloc(NUM_DIM,sizeof(float_type));
            assert_mem(strike_prob_vect);
            init_share_value_vect = (float_type *)calloc(NUM_DIM,sizeof(float_type));
            assert_mem(init_share_value_vect);
            //      sigma_matrix=gsl_matrix_calloc(NUM_DIM,NUM_DIM);
            sigma_matrix=(float_type **)malloc(NUM_DIM*sizeof(float_type *));
            assert_mem(sigma_matrix);
            for (i=0;i<NUM_DIM;i++){
                sigma_matrix[i]=(float_type *)malloc(NUM_DIM*sizeof(float_type));
                assert_mem(sigma_matrix[i]);
            }




        } else if( !strcmp(variable_name,"SIGMA") ) {
            for (i=0;i<NUM_DIM;i++){		    
                for (j=0;j<NUM_DIM;j++){		    
                    num_arg = sscanf(variable_value,"%[^ ,]%[,]%[^ \t\n]",temp_string,rubbish,variable_value);	   
                    if(num_arg == 3 || num_arg == 1 && i==NUM_DIM-1) {
                        sigma_matrix[i][j]=((float_type)floor(atof(temp_string)*10000)/10000);  //this make a differnece for the bad cas
                        //sigma_matrix[i][j]=(float_type)atof(temp_string);

                    }else{
                        fprintf(stderr, "\nERROR: Failed to read SIGMA properly from %s file.\n",config_file);
                        exit(EXIT_FAILURE);
                    } 
                }
            }
        } else if( !strcmp(variable_name,"INIT_SHARE_VALUE") ) {
            for (i=0;i<NUM_DIM;i++){
                num_arg = sscanf(variable_value,"%[^ ,]%[,]%[^ \t\n]",temp_string,rubbish,variable_value);	   
                if(num_arg == 3 || num_arg == 1 && i==NUM_DIM-1) {
                    init_share_value_vect[i]=(float_type)atof(temp_string);
                }else{
                    fprintf(stderr, "\nERROR: Failed to read PROB properly from %s file.\n",config_file);
                    exit(EXIT_FAILURE);
                } 
            }

        } else if( !strcmp(variable_name,"T") ) {
            T      = (float_type)atof(variable_value);

        } else if( !strcmp(variable_name,"RATE") ) {
            RATE      = (float_type)atof(variable_value);

        } else if( !strcmp(variable_name,"STRIKE") ) {
            for (i=0;i<NUM_DIM;i++){
                num_arg = sscanf(variable_value,"%[^ ,]%[,]%[^ \t\n]",temp_string,rubbish,variable_value);	   
                if(num_arg == 3 || num_arg == 1 && i==NUM_DIM-1) {
                    strike_prob_vect[i]=(float_type)atof(temp_string);
                }else{
                    fprintf(stderr, "\nERROR: Failed to read STRIKE properly from %s file.\n",config_file);
                    exit(EXIT_FAILURE);
                } 
            }



        } else if( !strcmp(variable_name,"MAX_MIN") ) {
            MAX_MIN      = (ushort)atol(variable_value);

        } else if( !strcmp(variable_name,"CASH") ) {
            CASH      = (float_type)atof(variable_value);

        } else if( !strcmp(variable_name,"VERBOSE") ) {
            VERBOSE      = (ushort)atol(variable_value);

        } else {
            fprintf(stderr, "\nERROR: Unknown configuration option \"%s\"\n",variable_name);
            exit(EXIT_FAILURE);
        }


    }	/* while */

    EXP_RATE=EXP_FUNC(RATE*(T/NUM_LAYERS));
    //  EXP_RATE=pow(1+RATE,T/NUM_LAYERS);

    conf_set->OPT_EUROPEAN=OPT_EUROPEAN;	
    conf_set->OPT_AMERICAN=OPT_AMERICAN;	  
    conf_set->CALL_PUT=CALL_PUT;	
    conf_set->T=T;	     
    conf_set->NUM_LAYERS=NUM_LAYERS;	     
    conf_set->NUM_DIM=NUM_DIM;	     
    conf_set->MAX_MIN=MAX_MIN;	     
    conf_set->RATE=RATE;	     
    conf_set->EXP_RATE=EXP_RATE;	     
    conf_set->CASH=CASH;	     
    conf_set->VERBOSE=VERBOSE;	     
    conf_set->share_mult_vect=share_mult_vect; 
    conf_set->eu_prob_vect=eu_prob_vect;	  
    conf_set->strike_prob_vect=strike_prob_vect;	     
    conf_set->init_share_value_vect=init_share_value_vect;
    conf_set->sigma_matrix=sigma_matrix;
    if(conf_set->VERBOSE){
        printf("T                       = %.4f\n",conf_set->T	    );
        printf("NUM_LAYERS        = %u\n",conf_set->NUM_LAYERS	    );
        print_float_type_matrix(sigma_matrix,NUM_DIM,NUM_DIM,"\nSIGMA=[");
        printf("\nOPT_EUROPEAN    = %u\n",conf_set->OPT_EUROPEAN   );
        printf("OPT_AMERICAN= %u\n",conf_set->OPT_AMERICAN   );
        printf("CALL_PUT= %u\n",conf_set->CALL_PUT   );
        printf("NUM_LAYERS= %u\n",conf_set->NUM_LAYERS	    );
        printf("NUM_DIM= %u\n",conf_set->NUM_DIM	    );
        printf("RATE= %.4f\n",conf_set->RATE	    );
        printf("EXP_RATE= %.4f\n",conf_set->EXP_RATE	    );
        printf("MAX_MIN= %u\n",conf_set->MAX_MIN	    );
        printf("CASH= %.4f\n",conf_set->CASH	    );	
        printf("VERBOSE= %u",conf_set->VERBOSE	    ); 	
        printf("\nSTRIKE=");
        for(i=0;i<NUM_DIM;i++){
            printf("%.4f,",conf_set->strike_prob_vect[i]);
        }
        printf("\nINIT_SHARE_VALUE	 =");
        for(i=0;i<NUM_DIM;i++){
            printf("%.4f,",conf_set->init_share_value_vect[i]);
        }
    }
}

void  prinf_float_type(char *head,float_type val,ushort frac_digits){
    char str[80];
    char tmp[10];
    sprintf(str,"%s",head);
    strcat (str,"%.");
    sprintf(tmp,"%d",frac_digits);
    strcat (str,tmp);
    strcat (str,PRNT_FRM);
    printf(str,val); 
}
void  fprintf_float_type(FILE *fid,char *head,float_type val,ushort frac_digits){
    char str[80];
    char tmp[10];
    sprintf(str,"%s",head);
    strcat (str,"%.");
    sprintf(tmp,"%d",frac_digits);
    strcat (str,tmp);
    strcat (str,PRNT_FRM);
    fprintf(fid,str,val); 
}

void prinf_float_type_hex(char *head,float_type val){
    char str[80];
    union {
        float_type f;
        char c[sizeof(float_type)]; // Edit: changed to this
    } prnt_union;
    prnt_union.f=val;
    sprintf(str,"%s",head);
    //  strcat (str," =");
    printf(str); 
    for ( int i = sizeof(float_type)-1; i>=0 ; --i ) printf( "%02X" , prnt_union.c[i] & 0x00FF );
}



void print_gsl_matrix(gsl_matrix *m){
    ulong r=m->size1;
    ulong c=m->size2;

    printf("\n");
    for(int i=0;i<r;i++){
        printf("\t");
        for(int j=0;j<c;j++){
            printf("%.18f,",gsl_matrix_get(m,i,j));
        }
        printf(";\n");
    }
}
void print_gsl_vector(gsl_vector *v){
    ulong r=v->size;

    for(int i=0;i<r;i++){
        printf("%.18f,",gsl_vector_get(v,i));
    }
    printf("]\n");

}

void print_float_type_matrix(float_type  **m,const ulong row,const  ulong col, char *head){
    ulong r=row;
    ulong c=col;

    printf("%s=",head);
    for(int i=0;i<r;i++){
        printf("\t");
        for(int j=0;j<c;j++){
            prinf_float_type(", ",m[i][j],PRNT_PREC);;
        }
        printf(";\n");
    }
}
void print_float_type_vector(float_type *v,ulong dim,char * head){
    ulong r=dim;
    printf("%s",head);
    for(int i=0;i<r;i++){
        prinf_float_type(", ",v[i],PRNT_PREC);;
    }
    printf("]\n");
}

void convert_to_gls_mat(float_type **m,ulong rows,ulong cols,gsl_matrix *m_gsl){
    for(int i=0;i<rows;i++){
        for(int j=0;j<cols;j++){
            gsl_matrix_set(m_gsl,i,j,(double)m[i][j]);
        }
    }
}
void top_tb_gen(config_set *conf_set,ushort num_dim,float_type *init_share_d0,float_type **share_mult_vect, opt_price_params *opp) {
    FILE *fid_tb=NULL;
    int i;
    float_type aa;
    char binString[PRECISION+2];
    printf("\n######### WRITING TO TEST FILE ########\n");
    fid_tb=fopen("opt_pricer_TB_data.dat","w");
    fprintf(fid_tb,"%d\n",conf_set->MAX_MIN);
    opp->max_min = conf_set->MAX_MIN;
    fprintf(fid_tb,"%s\n",byte_to_binary(conf_set->NUM_LAYERS)); //this ensure it is not casted into a double
    for (i=num_dim-1;i>=0;i--){
        aa=init_share_d0[i];
        opp->init_share[i] = aa;
        fp2bin(aa,binString);
        fprintf(fid_tb,"%s",binString);
        printf("init_share[%d] = %.4f =",i,aa);
        prinf_float_type_hex(" ",aa);
        printf("=%s\n",binString);
    }
    printf("\n");
    fprintf(fid_tb,"\n");
    for (i=0;i<num_dim;i++){
        aa=conf_set->strike_prob_vect[i];
        fp2bin(aa,binString);
        fprintf(fid_tb,"%s",binString);
        opp->strike[i] = aa;
        printf("strike[%d]     = %.4f =",i,aa);
        prinf_float_type_hex(" ",aa);
        printf("=%s\n",binString);  
    }
    printf("\n");
    fprintf(fid_tb,"\n");
    aa=conf_set->CASH;
    opp->cash = aa;
    fp2bin(aa,binString);
    fprintf(fid_tb,"%s",binString);
    printf("cash[%d]      = %.4f =",i,aa);
    prinf_float_type_hex(" ",aa);
    printf("=%s\n",binString);  
    printf("\n");
    fprintf(fid_tb,"\n");
    for (i=num_dim-1;i>=0;i--){
        for (int j=num_dim-1;j>=0;j--){
            aa=conf_set->share_mult_vect[i][j+1]/share_mult_vect[i][j];
            opp->init_step[i][j]=aa;
            fp2bin(aa,binString);
            fprintf(fid_tb,"%s",binString);
            printf("init_step%d[%d]  = %.4f =",i,j,aa);
            prinf_float_type_hex(" ",aa);
            printf("=%s\n",binString);  
        }
    }
    printf("\n");
    fprintf(fid_tb,"\n");

    for (int j=num_dim-1;j>=0;j--){
        aa=1/(conf_set->share_mult_vect[j][0]);
        opp->init_step_next[j] = aa;
        fp2bin(aa,binString);
        fprintf(fid_tb,"%s",binString);
        printf("init_steps_next[%d] = %.4f =",j,aa);
        prinf_float_type_hex(" ",aa);
        printf("=%s\n",binString);  
    }

    printf("\n");
    fprintf(fid_tb,"\n");

    for (int j=num_dim;j>=0;j--){
        aa=(1/conf_set->EXP_RATE)*conf_set->eu_prob_vect[j];
        opp->mult_coeff[j] = aa;
        fp2bin(aa,binString);
        fprintf(fid_tb,"%s",binString);
        printf("mult_coeff[%d] = %.4f =",j,aa);
        prinf_float_type_hex(" ",aa);
        printf("=%s\n",binString);  
    }

    printf("\n");
    fprintf(fid_tb,"\n");

    for (int j=num_dim-1;j>=0;j--){
        aa=1/(conf_set->share_mult_vect[j][0]);
        opp->shares_coeff[j] = aa;
        fp2bin(aa,binString);
        fprintf(fid_tb,"%s",binString);
        printf("shares_coeff(eu_prob,q)[%d] = %.4f =",j,aa);
        prinf_float_type_hex(" ",aa);
        printf("=%s\n",binString);  
    }

    printf("\n");
    fprintf(fid_tb,"\n");
    fclose(fid_tb);

    //exit(EXIT_FAILURE);
}

/* ======================= */
/* Load configuration file */
/* ======================= */
void load_config_file(char *config_file, config_set *conf_set)
{
    FILE *fid						= NULL;
    printf("Loading configuration info...\n");
    fid = fopen(config_file,"r");
    if(fid == NULL) {
        fprintf(stderr, "\nERROR: Could not open <%s> file.",config_file);
        fprintf(stderr, "\n       Need to specify valid configuration file.\n");
        exit(EXIT_FAILURE);
    }


    read_config(fid,conf_set,config_file);
    fclose(fid);
}
