/***********************************************************/
/* fp2bin.c: Convert IEEE double to binary string          */
/*                                                         */
/* Rick Regan, http://www.exploringbinary.com              */
/*                                                         */
/***********************************************************/
#include <string.h>
#include <math.h>
#include "fp2bin.h"

void fp2bin_i(double fp_int, char* binString)
{
  int bitCount = 0;
  int i;
  char binString_temp[FP2BIN_STRING_MAX];

  do
    {
      binString_temp[bitCount++] = '0' + (int)fmod(fp_int,2);
      fp_int = floor(fp_int/2);
    } while (fp_int > 0);

  /* Reverse the binary string */
  for (i=0; i<bitCount; i++)
    binString[i] = binString_temp[bitCount-i-1];

  binString[bitCount] = 0; //Null terminator
}

void fp2bin_f(double fp_frac, char* binString)
{
  int bitCount = 0;
  double fp_int;

  while (fp_frac > 0)
    {
      fp_frac*=2;
      fp_frac = modf(fp_frac,&fp_int);
      binString[bitCount++] = '0' + (int)fp_int;
    }
  binString[bitCount] = 0; //Null terminator
}


void fp2bin(float fp, char* binString)
{
  double fract;
  int exp2;
  int signbit; 
  if (fp==0){ 
    strcpy(binString,  "00"); //this take care of the extra two bits used in the flopoco representation(special cases) 
  } else{
  strcpy(binString,  "01"); //this take care of the extra two bits used in the flopoco representation(special cases)
 }
  /* Separate integer and fractional parts */
  fract = frexp( fp, &exp2); /* split fval into fraction and binary exponent */

  signbit = fract < 0; /* detect negative value and get absolute value of fract */
  if (signbit) fract = -fract;
  fract = 2*fract - 1.0;
  exp2 = 126 + exp2; 

  //  putchar(signbit ? '1' : '0'); /* Print the sign bit and a space */
  //  putchar(' '); 
  if (signbit){
    strcat(binString,"1");
  }else{
    strcat(binString,"0");
  }

  /* Print the 7 least bits of exp2 in binary form: */

  for (int mask = 128; mask != 0; mask = mask >> 1)
    {
      //      putchar( exp2&mask ? '1' : '0' );
      if (exp2&mask){
	strcat(binString,"1");
      }else{
	strcat(binString,"0");
      }
    }

  //  putchar(' '); // output a space

  /* Print 24 bits of fraction */

  for (int n=0; n<23; ++n)
    {
      fract = fract*2.0;
      if (fract >= 1.0)
	{
	  fract -= 1.0;
	  //	  putchar('1');
	  strcat(binString,"1");
	}
      else //putchar('0');
	strcat(binString,"0");
    }
  //  putchar('\n');


}
